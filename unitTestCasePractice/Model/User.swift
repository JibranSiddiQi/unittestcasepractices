//
//  User.swift
//  unitTestCasePractice
//
//  Created by Jibran SiddiQui on 10/01/2019.
//  Copyright © 2019 CodeSquareITSolution. All rights reserved.
//

import Foundation

struct User {
    let name: String
    let userName: String
    let email: String
    let address: String
    let designation: String
}
