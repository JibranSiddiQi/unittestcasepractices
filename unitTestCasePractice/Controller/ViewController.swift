//
//  ViewController.swift
//  unitTestCasePractice
//
//  Created by Jibran SiddiQui on 10/01/2019.
//  Copyright © 2019 CodeSquareITSolution. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // MARK:  IBOutlets
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    // MARK:  Variables
    var loginService: LoginService!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
         loginService = LoginServiceDelegate(delegate: self)
    }
    @IBAction func tappedLogin(_ sender: UIButton) {
        loginService.login(withUsername: username.text, password: password.text)
    }
}

extension ViewController: LoginActionService {
    func loginSuccessfull(withUser user: User) {
        showAlert(withTitle: "Success", message: "Hello \(user.name)")
    }
    
    func handle(error: Error) {
        showAlert(withTitle: "Error", message: error.localizedDescription)
    }
}
