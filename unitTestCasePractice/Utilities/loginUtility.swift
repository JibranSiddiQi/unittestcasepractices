//
//  loginUtility.swift
//  unitTestCasePractice
//
//  Created by Jibran SiddiQui on 10/01/2019.
//  Copyright © 2019 CodeSquareITSolution. All rights reserved.
//

import Foundation

enum LoginServiceError: LocalizedError {
    case invalidCredentials
    case nilData
    case wrongStatusCode
    case unknown
    
    var errorDescription: String? {
        var errorString:String? = nil
        switch self {
        case .invalidCredentials:
            errorString = "The user credentials are invalid"
        case .nilData:
            errorString = "Failed to fetch your data, please try again later"
        case .wrongStatusCode:
            errorString = "Wrong status code received, please try again later"
        default:
            errorString = "Something went wrong please try again later"
        }
        
        return errorString
    }
}

enum LoginFormValidationError: LocalizedError {
    case invalidUsernameLength
    case invalidPasswordLength
    case invalidPasswordCharacters
    
    var errorDescription: String? {
        var errorString:String? = nil
        switch self {
        case .invalidUsernameLength:
            errorString = "Your username should have atleast 2 and atmost 10 characters"
        case .invalidPasswordLength:
            errorString = "Your password should be of atleast 3 characters with atleast one upercase letter and one decimal digit"
        case .invalidPasswordCharacters:
            errorString = "Your password should have atleast one upercase letter and one decimal digit"
        }
        
        return errorString
    }
}

extension LoginFormValidationError: Equatable {}

func ==(lhs: LoginFormValidationError, rhs: LoginFormValidationError) -> Bool {
    switch (lhs, rhs) {
    case (.invalidUsernameLength, .invalidUsernameLength): fallthrough
    case (.invalidPasswordLength, .invalidPasswordLength): fallthrough
    case (.invalidPasswordCharacters, .invalidPasswordCharacters):
        return true
    default:
        return false
    }
}

func ==(lhs: LocalizedError, rhs: LocalizedError)  ->  Bool{
    return lhs.errorDescription == rhs.errorDescription && lhs.failureReason == rhs.failureReason && lhs.helpAnchor == rhs.helpAnchor && lhs.recoverySuggestion == rhs.recoverySuggestion && lhs.localizedDescription == rhs.localizedDescription
}
