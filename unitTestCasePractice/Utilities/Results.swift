//
//  Results.swift
//  unitTestCasePractice
//
//  Created by Jibran SiddiQui on 10/01/2019.
//  Copyright © 2019 CodeSquareITSolution. All rights reserved.
//

import Foundation
import UIKit

public enum Result<T> {
    case success(T)
    case failure(Error)
    
    func resolve() throws -> T {
        switch self {
        case .success(let T):
            return T
        case .failure(let error):
            throw error
        }
    }
}
